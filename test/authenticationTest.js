
/*
  {
      year : 2017
  }
*/
var supertest = require("supertest");
var should = require("should");
var request = require("request")

// This agent refers to PORT where program is running.
var server = supertest.agent("http://127.0.0.1:3000");

describe("routes_v2", function(){
  describe("authentication.js", function(){
    describe("/api/v1/auth/signIn", function(){

      //first test case...for status etc.
      it("status is 200", function(done){
          server
          .post('/api/v1/auth/signIn')
          .send({username: "rishikesh.agrawani@relevancelab.com", password: "Pass@123", captcha: "1234"})
          .expect("Content-type",/json/)
          .expect(200)
          .end(function(err,res){
            // res.status.should.equal(200);
            // res.body.error.should.equal(false);
            // res.body.data.should.equal(40);
            console.log(res.body)
            
          });
          done();
      })
    })

    describe("/api/v1/auth/signout", function(){

      //first test case...for status etc.
      it("status is 200", function(done){
          server
          .post('/api/v1/auth/signout')
          .send({})
          .expect("Content-type",/json/)
          .expect(200)
          .end(function(err,res){
            // res.status.should.equal(200);
            // res.body.error.should.equal(false);
            // res.body.data.should.equal(40);
            console.log(res.body)
            
          });
          done();
      })
    })
  })  
})


